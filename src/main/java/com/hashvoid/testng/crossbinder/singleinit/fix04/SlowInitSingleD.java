/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.testng.crossbinder.singleinit.fix04;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import rd.crossbinder.hod.Initializable;
import rd.crossbinder.hod.Lazy;
import rd.crossbinder.hod.Singleton;

/**
 * @author randondiesel
 *
 */

@Singleton @Lazy
public class SlowInitSingleD implements Initializable, SlowFaceD {

	private static Set<String> idSet = new HashSet<>();

	public SlowInitSingleD() {
		idSet.add(UUID.randomUUID().toString());
	}

	@Override
	public void initialize() {
		// Put in a delay of 2 sec
		try {
			Thread.sleep(2000);
		}
		catch (InterruptedException exep) {
			// TODO Auto-generated catch block
			exep.printStackTrace();
		}
	}

	@Override
	public void callme() {
		// NOOP
	}

	@Override
	public Set<String> getIdSet() {
		return idSet;
	}
}
