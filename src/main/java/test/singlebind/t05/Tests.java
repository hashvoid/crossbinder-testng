/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package test.singlebind.t05;

import org.testng.Assert;
import org.testng.annotations.Test;

import rd.testng.Trace;
import rd.testng.XbinderTest1;

/**
 * @author randondiesel
 *
 */

public class Tests extends XbinderTest1 {

/**
 * If a singleton does not implement any interface, then the singleton is added to crossbinder as a
 * shadow singleton.
 *
 * @param tctxt
 */

	@Test
	public void execute1() {
		Object value = Trace.getInst().get(WbmaShad.class.getName());
		Assert.assertNotNull(value);
		Assert.assertEquals(value, "created");
	}

/**
 * If a singleton implements multiple interfaces, and all the interfaces are annotated with
 * @NonBindable, then the singleton is added to crossbinder as a shadow singleton.
 *
 * @param tctxt
 */

	@Test
	public void execute2() {
		Wbma1 obj1 = crossbinder().locator().get(Wbma1.class);
		Wbma2 obj2 = crossbinder().locator().get(Wbma2.class);

		Object value = Trace.getInst().get(WbmaSing.class.getName());

		Assert.assertNotNull(value);
		Assert.assertEquals(value, "created");

		Assert.assertNull(obj1);
		Assert.assertNull(obj2);
	}
}
