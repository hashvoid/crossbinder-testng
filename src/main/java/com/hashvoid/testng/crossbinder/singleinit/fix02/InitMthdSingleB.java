/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.testng.crossbinder.singleinit.fix02;

import rd.crossbinder.hod.Initialize;
import rd.crossbinder.hod.Singleton;
import rd.testng.Trace;

/**
 * @author randondiesel
 *
 */

@Singleton
public class InitMthdSingleB implements FaceB2 {

	@Initialize
	public void doInit1(Object someObj) {
		Trace.getInst().set(InitMthdSingleB.class.getName(), "initialized1");
	}

	@Initialize
	public Object doInit2() {
		Trace.getInst().set(InitMthdSingleB.class.getName(), "initialized2");
		return 23;
	}

	@Initialize
	public void doInit0() {
		Trace.getInst().set(InitMthdSingleB.class.getName(), "initialized3");
	}
}
