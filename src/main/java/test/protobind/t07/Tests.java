/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package test.protobind.t07;

import org.testng.Assert;
import org.testng.annotations.Test;

import rd.testng.XbinderTest1;

/**
 * If a prototype implements multiple un-annotated interfaces, derives from a base class, that in
 * turn implements multiple un-annotated interfaces, then the prototype can be referenced using all
 * the interfaces in the hierarchy. Furthermore, a new instance of the prototype is obtained for
 * each reference made.
 * <p>
 *
 * @author randondiesel
 *
 */

public class Tests extends XbinderTest1 {

	@Test
	public void execute() {
		Salmb1 obj1 = crossbinder().locator().get(Salmb1.class);
		Salmb2 obj2 = crossbinder().locator().get(Salmb2.class);
		Salmb3 obj3 = crossbinder().locator().get(Salmb3.class);

		Assert.assertNotNull(obj1);
		Assert.assertNotNull(obj2);
		Assert.assertNotNull(obj3);
		Assert.assertNotSame(obj1, obj2);
		Assert.assertNotSame(obj2, obj3);
	}
}
