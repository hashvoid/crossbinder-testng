/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package rd.testng;

import java.util.List;
import java.util.Map;

import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestNGMethod;
import org.testng.xml.XmlSuite;

/**
 * @author randondiesel
 *
 */

public class DieselReporter implements IReporter {

	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outDir) {
		System.out.println("diesel reporter invoked");
		for(ISuite suite : suites) {
			System.out.println(suite.getName());
			Map<String, ISuiteResult> resultMap = suite.getResults();
			for(String key : resultMap.keySet()) {
				ISuiteResult result = resultMap.get(key);
				IResultMap resMap = result.getTestContext().getPassedTests();
				for(ITestNGMethod mthd : resMap.getAllMethods()) {
					mthd.getConstructorOrMethod().getMethod();
				}
			}
		}
	}
}
