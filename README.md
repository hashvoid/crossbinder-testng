# Crossbinder Unit Tests

This is a test suite for validating the capabilities of the crossbinder dependency injection and lifecycle management container. Crossbinder is a project under [Hashvoid](http://www.hashvoid.com/crossbinder). This test suite is written using [TestNG](http://www.testng.org) as the underlying framework.

# How the Test Suite is Structured

If you are following the project directory structure recommended by Maven, it would be customary to place all code pertaining to unit tests under the ```src\test\java``` folder, and then use the surefire plugin to execute these tests while performing a Maven build. We decided to follow a different approach, which can be summarized as follows:

1. The first deviation is that we decided to keep the test cases outside of the main project directory of crossbinder. This gives us more freedom to update and work on the test cases without having to update the main crossbinder releases. The test suite is therefore written as a separate Maven project. Furthermore, we have also shared the source code on gitlab, while the main crossbinder code is being maintained on github.

1. The second deviation is that we decided to keep the test case execution independent of the Maven build process. To that extent, we plan to execute the test suite from a command line without running Maven. Of course, we still use Maven for dependency resolution and building the test suite.

1. The thrird deviation is that we do not expect the test suite executors to understand testng specific constructs to launch the test suite from the command line (e.g. using something like ```java org.testng.TestNG testng.xml```). So we have provided a regular Java main method that internally launches and manages testng alongwith the necessary configuration for test execution.


# Executing the Test Suite

Pending details
