/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package test.protobind.t05;

import org.testng.Assert;
import org.testng.annotations.Test;

import rd.testng.Trace;
import rd.testng.XbinderTest1;

/**
 * If a prototype does not implement any interface, or implements only interfaces that are
 * annoatated with @NonBindable, then the prototype is not added to crossbinder.
 * <p>
 *
 * @author randondiesel
 *
 */

public class Tests extends XbinderTest1 {

	@Test
	public void execute() {
		Object val1 = Trace.getInst().get(ZvobzProto.class.getName());
		Object val2 = Trace.getInst().get(FdolbProt.class.getName());

		FdolbNoBind1 obj1 = crossbinder().locator().get(FdolbNoBind1.class);
		FdolbNoBind2 obj2 = crossbinder().locator().get(FdolbNoBind2.class);

		Assert.assertNull(val1);
		Assert.assertNull(val2);
		Assert.assertNull(obj1);
		Assert.assertNull(obj2);
	}
}
