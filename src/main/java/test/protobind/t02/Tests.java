/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package test.protobind.t02;

import org.testng.Assert;
import org.testng.annotations.Test;

import rd.testng.XbinderTest1;

/**
 *
 * @author randondiesel
 *
 */

public class Tests extends XbinderTest1 {

/**
 * If a prototype implements multiple interfaces, none of which are annotated with either @Bindable
 * or @NonBindable, then the prototype can be referenced through all interfaces. Furthermore, a
 * different instance of the prototype is referenced through each of these interfaces.
 */

	@Test
	public void execute1() {
		Wzvyn1 obj1 = crossbinder().locator().get(Wzvyn1.class);
		Wzvyn2 obj2 = crossbinder().locator().get(Wzvyn2.class);

		Assert.assertNotNull(obj1);
		Assert.assertNotNull(obj2);
		Assert.assertNotSame(obj1, obj2);
	}

/**
 * A new instance of a prototype is created every time it is referenced, even with the same
 * interface.
 */

	@Test
	public void execute2() {
		Wzvyn1 obj1 = crossbinder().locator().get(Wzvyn1.class);
		Wzvyn1 obj2 = crossbinder().locator().get(Wzvyn1.class);

		Assert.assertNotNull(obj1);
		Assert.assertNotNull(obj2);
		Assert.assertNotSame(obj1, obj2);
	}

}
