/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package test.protobind.t04;

import org.testng.Assert;
import org.testng.annotations.Test;

import rd.testng.XbinderTest1;

/**
 * If a prototype implements multiple interfaces, and some of the interfaces are annotated
 * with @NonBindable, then the prototype can be references through the un-annotated interfaces only.
 * <p>
 *
 * @author randondiesel
 *
 */

public class Tests extends XbinderTest1 {

	@Test
	public void execute() {
		Nuips obj1 = crossbinder().locator().get(Nuips.class);
		NuipsNoBind obj2 = crossbinder().locator().get(NuipsNoBind.class);

		Assert.assertNotNull(obj1);
		Assert.assertNull(obj2);
	}
}
