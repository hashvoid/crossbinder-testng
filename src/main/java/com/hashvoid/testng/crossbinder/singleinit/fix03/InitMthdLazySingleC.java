/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.testng.crossbinder.singleinit.fix03;

import rd.crossbinder.hod.Initialize;
import rd.crossbinder.hod.Lazy;
import rd.crossbinder.hod.Singleton;
import rd.testng.Trace;

/**
 * @author randondiesel
 *
 */

@Singleton @Lazy
public class InitMthdLazySingleC implements FaceC2 {

	@Initialize
	public void doInit1(Object someObj) {
		Trace.getInst().set(InitMthdLazySingleC.class.getName(), "initialized1");
	}

	@Initialize
	public Object doInit2() {
		Trace.getInst().set(InitMthdLazySingleC.class.getName(), "initialized2");
		return 23;
	}

	@Initialize
	public void doInit0() {
		Trace.getInst().set(InitMthdLazySingleC.class.getName(), "initialized3");
	}

	@Override
	public void callme() {
		// NOOP
	}
}
