/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package rd.testng;

import java.util.List;
import java.util.logging.Logger;

import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlPackage;

import rd.classpath.ScanPath;
import rd.crossbinder.hod.Crossbinder;

/**
 *
 * @author randondiesel
 *
 */

public abstract class XbinderTest2 {

	private static final Logger LOGGER = Logger.getLogger(XbinderTest2.class.getName());

	private Crossbinder xbind;
	private Throwable   startError;

	@BeforeTest
	public final void setup(ITestContext tctxt) {
		LOGGER.info(String.format("setting up crossbinder for tests: %s", tctxt.getName()));
		Trace.getInst().reset();

		ScanPath sp = new ScanPath();

		List<XmlPackage> pkgs = tctxt.getCurrentXmlTest().getPackages();
		for(XmlPackage pkg : pkgs) {
			for(String name : pkg.getInclude()) {
				sp.includePackage(name);
			}
			for(String name : pkg.getExclude()) {
				sp.excludePackage(name);
			}
		}

		List<XmlClass> clss = tctxt.getCurrentXmlTest().getClasses();
		for(XmlClass cls : clss) {
			sp.includeClass(cls.getName());
		}

		xbind = Crossbinder.create().scanPath(sp);
		try {
			xbind.start();
		}
		catch(Throwable th) {
			startError = th;
		}
	}

	@AfterTest
	public final void teardown(ITestContext tctxt) {
		if(xbind != null) {
			xbind.stop();
		}
	}

	protected final void rethrowSetupError() throws Throwable {
		if(startError != null) {
			//startError.printStackTrace();
			throw startError;
		}
	}

	protected final Crossbinder crossbinder() {
		return xbind;
	}
}
