/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package test.singlebind.t07;

import org.testng.Assert;
import org.testng.annotations.Test;

import rd.testng.XbinderTest1;

/**
 * If a singleton implements multiple un-annotated interfaces, derives from a base class, that in
 * turn implements multiple un-annotated interfaces, then the singleton can be referenced using all
 * the interfaces in the hierarchy. Furthermore, the same instance of the singleton is referenced at
 * all times.
 *
 * @author randondiesel
 */

public class Tests extends XbinderTest1 {

	@Test
	public void execute() {
		IHixp1 obj1 = crossbinder().locator().get(IHixp1.class);
		IHixp2 obj2 = crossbinder().locator().get(IHixp2.class);
		IHixp3 obj3 = crossbinder().locator().get(IHixp3.class);

		Assert.assertNotNull(obj1);
		Assert.assertNotNull(obj2);
		Assert.assertNotNull(obj3);
		Assert.assertSame(obj1, obj2);
		Assert.assertSame(obj2, obj3);
	}
}
