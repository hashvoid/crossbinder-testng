/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.testng.crossbinder.protoinit.fix01;

import rd.crossbinder.hod.Inject;
import rd.crossbinder.hod.Lazy;
import rd.crossbinder.hod.Singleton;

/**
 * @author randondiesel
 *
 */

@Singleton @Lazy
public class SingleA implements SingleFace {

	@Inject
	private ProtoFaceA1 pface1;

	@Inject
	private ProtoFaceA2 pface2;

	@Override
	public String getProtoId1() {
		return pface1.getId();
	}

	@Override
	public String getProtoId2() {
		return pface2.getId();
	}
}
