/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package test.protobind.t01;

import org.testng.Assert;
import org.testng.annotations.Test;

import rd.testng.Trace;
import rd.testng.XbinderTest1;

/**
 * If a prototype entity is not annotated with @Prototype, it is not bound with Crossbinder.
 *
 * @author randondiesel
 *
 */

public class Tests extends XbinderTest1 {

	@Test
	public void execute() {
		Object value = Trace.getInst().get(NotProtJzmht.class.getName());
		Jzmht1 obj1 = crossbinder().locator().get(Jzmht1.class);
		Jzmht2 obj2 = crossbinder().locator().get(Jzmht2.class);
		Assert.assertNull(value);
		Assert.assertNull(obj1);
		Assert.assertNull(obj2);
	}
}
